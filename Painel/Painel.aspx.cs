﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Painel
{
    public partial class Painel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnProximo1_Click(object sender, EventArgs e)
        {
            Painel1.Visible = false;
            Painel2.Visible = true;
            Painel3.Visible = false;

        }

        protected void btnProximo2_Click(object sender, EventArgs e)
        {
            Painel1.Visible = false;
            Painel2.Visible = false;
            Painel3.Visible = true;
            

        }

        protected void btnVoltar1_Click(object sender, EventArgs e)
        {
            Painel1.Visible = true;
            Painel2.Visible = false;
            Painel3.Visible = false;
        }

        protected void btnVoltar2_Click(object sender, EventArgs e)
        {
            Painel1.Visible = false;
            Painel2.Visible = true;
            Painel3.Visible = false;
        }

        protected void ButtonEnviar_Click(object sender, EventArgs e)
        {
            lblResultadoNaTela.Text = "Dados Enviados com Sucesso!!!";
        }
    }
}