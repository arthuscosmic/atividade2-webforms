﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Painel.aspx.cs" Inherits="Painel.Painel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Painel de Informações</title>
    <style>
        body{
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="PainelPrincipal" runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="Black" Width="300px" Padding="10">
    <asp:Panel ID="Painel1" runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#333" Padding="5" Margin="5">
        <h3>Informações Pessoais</h3>
        <table>
            <tr>
                <td><asp:Label runat="server" Text="Nome Completo:" ID="lblNome"></asp:Label></td>
                <td><asp:TextBox ID="txtNome" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" Text="Gênero:" ID="lblGenero"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownGenero" runat="server">
                        <asp:ListItem Text="Masculino" Value="Masculino"></asp:ListItem>
                        <asp:ListItem Text="Feminino" Value="Feminino"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label runat="server" Text="Celular:" ID="lblCelular"></asp:Label></td>
                <td><asp:TextBox ID="txtCelular" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <asp:Button ID="btnProximo1" runat="server" Text="Próximo" OnClick="btnProximo1_Click"  />
    </asp:Panel>
    <asp:Panel ID="Painel2" runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#333" Padding="5" Margin="5" Visible="False">
       <h3>Detalhes do Endereço</h3>
        <table>
            <tr>
                <td><asp:Label runat="server" Text="Endereço:" ID="lblEndereco"></asp:Label></td>
                <td><asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" Text="Cidade:" ID="lblCidade"></asp:Label></td>
                <td><asp:TextBox ID="txtCidade" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" Text="CEP:" ID="lblCEP"></asp:Label></td>
                <td><asp:TextBox ID="txtCEP" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <asp:Button ID="btnVoltar1" runat="server" Text="Voltar" OnClick="btnVoltar1_Click"  />
        <asp:Button ID="btnProximo2" runat="server" Text="Próximo" OnClick="btnProximo2_Click"  />
    </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="Painel3" runat="server" BorderStyle="Solid" BorderWidth="1" BorderColor="#333" Padding="5" Margin="5" Width="294px" Visible="False">
         <h3>Área de Login</h3>
        <table>
            <tr>
                <td><asp:Label runat="server" Text="Usuário:" ID="lblUsuario"></asp:Label></td>
                <td><asp:TextBox ID="txtUsuario" runat="server" ></asp:TextBox></td>
            </tr>
            <tr>
                <td><asp:Label runat="server" Text="Senha:" ID="lblSenha"></asp:Label></td>
                <td><input id="psdSenha" type="password" /></td>
            </tr>
        </table>
        <asp:Button ID="btnVoltar2" runat="server" Text="Voltar" OnClick="btnVoltar2_Click"  />
        <asp:Button ID="ButtonEnviar" runat="server" Text="Enviar" OnClick="ButtonEnviar_Click"  />
        <td><asp:Label runat="server" ID="lblResultadoNaTela"></asp:Label></td>
    </asp:Panel>
    </asp:Panel>
</asp:Panel>

    </form>
</body>
</html>
